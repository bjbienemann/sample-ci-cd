package br.com.bienemann.samplecicd.domain;

public class Person {

    private final String id;
    private final String name;
    private final String telephone;
    
    public Person(
        String id,
        String name,
        String telephone
    ) {
        this.id = id;
        this.name = name;
        this.telephone = telephone;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTelephone() {
        return telephone;
    }

}
