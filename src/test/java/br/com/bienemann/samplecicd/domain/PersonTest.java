package br.com.bienemann.samplecicd.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PersonTest {

    @Test
    void mustCreatePerson() {
        var id = "1234567890";
        var name = "Brian Bienemann";
        var telephone = "00999998888";

        Person person = new Person(
            id,
            name,
            telephone
        );

        assertEquals(id, person.getId());
        assertEquals(name, person.getName());
        assertEquals(telephone, person.getTelephone());
    }
    
}
