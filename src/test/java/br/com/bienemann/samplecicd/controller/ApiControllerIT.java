package br.com.bienemann.samplecicd.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(ApiController.class)
public class ApiControllerIT {
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	void deveRetornarHelloword() throws Exception {
		MvcResult mvcReturn = mvc.perform(MockMvcRequestBuilders.get("/"))
				.andExpect(status().isOk())
				.andReturn();
		
		String contentAsString = mvcReturn.getResponse().getContentAsString();
		
		assertThat(contentAsString).isEqualTo("Hello World!");
	}

}
